import React, {  useEffect, useState }  from "react";
import ButtonTemplate from "../../components/Button/Button";
import Accordion from '@material-ui/core/ExpansionPanel';
import AccordionSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpandMoreIcon from '@material-ui/icons/ExpandMore';
import { Checkbox } from '@material-ui/core';
import Paper from '@material-ui/core/Paper';
import Table from '@material-ui/core/Table';
import TableBody from '@material-ui/core/TableBody';
import TableCell from '@material-ui/core/TableCell';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import AddIcon from '@material-ui/icons/Add';
import TableContainer from '@material-ui/core/TableContainer';
import TableHead from '@material-ui/core/TableHead';
import { NavLink } from "react-router-dom";
import Pagination from "../../components/Pagination/Pagination";
import ItensPage from "../../components/ItensPage/ItensPage";
import TableRow from '@material-ui/core/TableRow';
import Typography from '@material-ui/core/Typography';
import Select from 'react-select';
import {SearchInput} from '../../components/SearchInput/SearchInput';
import { apiGetPessoas } from './actions.js'
import ButtonIconTemplate from "../../components/ButtonIcon/ButtonIcon";
import { ModalYN } from "../../components/ModalYN/ModalYN";
import Loading from "../../components/Loading/Loading"
import f from 'odata-filter-builder';
import "./Lista-1Page.css"

//https://www.npmjs.com/package/odata-query
//https://www.npmjs.com/package/odata-filter-builder

export default function Lista1Page(props) {

const [usersTable, setUsersTable] = useState([])
const [showModal, setShowModal] = useState(false)
const [odata, setOdata] = useState({
                                    select: ['name, id, email, systemRoleId'],
                                    expand: ['systemrole'],
                                    top: 10,
                                    skip: 0,
                                    count: true,
                                    })
const [filterOdata, setFilterOdata] = useState({})
const [check, setCheck] = useState(false)
const [pageTop, setPagetop] = useState(1)
const [pageItens, setItens] = useState(10)
const [ftName, setFtName] = useState('')
const [load, setLoad] = useState(true)

useEffect(()=>{
  async function get() {
    setLoad(true)
    let pes = await apiGetPessoas(odata)
    setUsersTable(pes)
    setLoad(false)
    console.log(props)
  } get()
}, [])

const nameFilter = (input) => {
  // const filter = f().contains(x => x.toLower('name'), `${input}`).toString();
  setFtName(input)
  setFilterOdata((prevState) => (
    { ...prevState, "Name": { contains: `${input}` }  
    }
  ));
  if((ftName==='') || (ftName===undefined)){
    delete filterOdata.Name
  }
}

const changePage = (page) => {
  odata.skip = ((page-1)*pageItens)
  setPagetop(page)
  updateTable()
}

const changeItens = (itens) => {
  odata.top = itens
  odata.skip = (0)
  setPagetop(1)
  setItens(itens)
  updateTable()
}

function statusFilter(event){
  if(event === true){
    setFilterOdata((prevState) => (
      { ...prevState, "status": true  
      }
    ));
  }
  if(event === false){
    if(filterOdata.status){
      delete filterOdata.status
    }
  }
  setCheck(event)
}

async function updateTable() {
  odata.filter = filterOdata
  setLoad(true)
  let pes = await apiGetPessoas(odata)
  setLoad(false)
  setUsersTable(pes)
}



function filter() {
  updateTable()
}

return(
    <>
     <h1>Pessoas</h1>

<Accordion>
    <AccordionSummary
      expandIcon={<ExpandMoreIcon />}
      aria-controls="panel1a-content"
      id="panel1a-header"
    >
    <Typography >Filtros</Typography>
    </AccordionSummary>
   
    <div className="ConteinerFiltro">
    <Typography>Setores</Typography>
    <Select 
             name="data"
             placeholder={'sem competências'}
            >
          </Select>
    <div >
        Mostrar apenas colaboradores ativados<Checkbox 
         checked={check}
        // onChange={e => setDesativados(e.target.checked )}
        onChange={e=> statusFilter(e.target.checked)}
        ></Checkbox>
      </div>
      <SearchInput className="searchInput"  placeHolder="Filtrar por nome" filter={nameFilter}></SearchInput>
      <ButtonTemplate className="btFilterAct" onClick={()=>filter()}>Filtrar</ButtonTemplate>
    </div>
  </Accordion>

<div className="flexArea">
  <ItensPage change={changeItens}/>

  <div className="btAddCrud">
    <NavLink to={{
              pathname: `/Crud-1/${'add'}`,
              state: {}
          }}> <ButtonTemplate buttonColor="blue" tonalidade="500" size="large"><AddIcon/>Adicionar</ButtonTemplate></NavLink>
  </div>
</div>

<Paper className="PaperTable" elevation={0}>
  {
    load ? <Loading/>:null
  }
  <TableContainer>
  <Table>
        <TableHead>
          <TableRow>
            <TableCell>Nome</TableCell>
            <TableCell>Cpf</TableCell>
            <TableCell>altura</TableCell>
            <TableCell>Peso</TableCell>
            <TableCell>E-mail</TableCell>
            <TableCell>Ações</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {( usersTable ).map((object, i)=>(
            <TableRow hover key={i}>
            <TableCell>{object.nome}</TableCell>
            <TableCell>{object.cpf}</TableCell>
            <TableCell>{object.altura}</TableCell>
            <TableCell>{object.peso}</TableCell>
            <TableCell>{object.email}</TableCell>
            <TableCell>
              <NavLink to= {`/Crud-1/${'1'}`}>
              <ButtonIconTemplate buttonColor="grey" tonalidade="800" icon={<EditIcon fontSize="large"/>}/>
              {/* <ButtonTemplate buttonColor="grey" variant="none" tonalidade="800" size="large"> <EditIcon fontSize="large"/> Editar </ButtonTemplate> */}
              </NavLink>
              <ButtonIconTemplate buttonColor="red" tonalidade="500" icon={<DeleteIcon fontSize="large"/>} onClick={()=> setShowModal(true)}/>
            </TableCell>
          </TableRow>
          ))}
          </TableBody>
          </Table>
  </TableContainer>
</Paper>

<ModalYN title="deseja mesmo deletar?" show={showModal} hide={()=>setShowModal(false)} yes={()=>console.log('y')} no={()=>console.log('n')}></ModalYN>


<div className="PagTable">
  <Pagination itensPage={10} total={200} InicialPage={1} page={pageTop} change={changePage}/>
</div>
    </>
)
}