import React, {  useState, useEffect }  from "react";
import Pagination from '@material-ui/lab/Pagination';

export default function PaginationComponent(props) {
    const [countItens, setCountItens] = useState(0)
    useEffect(()=>{
        setCountItens(Math.ceil(props.total/props.itensPage))
    })

    const handleChange = (event, value) => {
       props.change(value);
      };
    
return(
    <>
    <Pagination color="secondary"  count={countItens} page={props.page} shape="rounded" onChange={handleChange}/>
    </>
)
}