import React, { useState, useEffect } from 'react';
import {CircularProgress, LinearProgress} from '@material-ui/core';
import "./Loading.css"

export default function Loading(props){
    const [type, setType] = useState(true)

    useEffect(()=>{
        if(props.type == 'circle'){
            setType(true)
        }
        if(props.type == 'line'){
            setType(false)
        }
    },[])

return(
    <>
    <div className="backLoading">
    { type ? 
    <div className="circleLoading">
      <CircularProgress color="secondary" />
    </div> : <LinearProgress color="secondary"/>
    }
    </div>
    </>
)
}