import axios from 'axios';
import env from '../../env';
// import { authProvider, authenticationParameters } from '../provider/authProvider';

let api = axios.create({
  baseURL: env.BASE_URL_API,
  // headers: {
  //   Authorization: ``
  // }
})



// api.interceptors.request.use(config => {
//   if (window !== undefined) {
//     const bearer = localStorage.getItem("msal.idtoken");
//     config.headers['Authorization'] = `Bearer ${bearer}`;
//   }
//   return config;
// });

// api.interceptors.response.use(response => response, async err => {
//   if (err.response && err.response.status === 401) {
//       await authProvider.acquireTokenSilent(authenticationParameters);
//   }
//   return Promise.reject(err);
// });


export default api;
