import React from "react";
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';
import { green, purple, blue, amber, blueGrey, 
    brown, common, cyan, deepOrange, 
    deepPurple, grey,
    indigo, lightBlue, lightGreen, lime, orange,
    pink, red, teal, yellow } from '@material-ui/core/colors';


export default function ButtonTemplate(props) {
    function getTonalidade() {
        if(props.tonalidade) {
            if(!parseInt(props.tonalidade)){
                if(props.tonalidade==='A100'){
                    return {
                        color: props.tonalidade,
                        hover: props.tonalidade,
                        contrast:props.tonalidade }
                }
                if(props.tonalidade==='A200'){
                    return {
                        color: props.tonalidade,
                        hover: props.tonalidade,
                        contrast:props.tonalidade }
                }
                if(props.tonalidade==='A400'){
                    return {
                        color: props.tonalidade,
                        hover: props.tonalidade,
                        contrast: 700 }
                }
                if(props.tonalidade==='A700'){
                    return {
                        color: props.tonalidade,
                        hover: props.tonalidade,
                        contrast: 900 }
                }if(props.tonalidade==='success'){
                    return {
                        color: '#1BC5BD',
                        hover: '#1BC5BD',
                        contrast: '#000000' }
                     } else {
                    return {
                        color: 500,
                        hover: 600,
                        contrast: 700 }
                }
            } else {
                if(props.tonalidade>900){
                    return {
                        color: 500,
                        hover: 600,
                        contrast: 700 }
                } else {
                    return {
                        color: parseInt(props.tonalidade),
                        hover: parseInt(props.tonalidade)+100 > 900 ? 900 : parseInt(props.tonalidade)+100,
                        contrast: parseInt(props.tonalidade)+300 > 900 ? 900 :  parseInt(props.tonalidade)+200}
                }
                }
            } else {
                return {
                    color: 500,
                    hover: 600,
                    contrast: 700 }
            }
        }
    

    function getColor() {
        if(props.buttonColor === 'red'){
            return red
        }
        if(props.buttonColor === 'blue'){
            return blue
        }
        if(props.buttonColor === 'green'){
            return green
        }
        if(props.buttonColor === 'purple'){
            return purple
        }
        if(props.buttonColor === 'amber'){
            return amber
        }
        if(props.buttonColor === 'blueGrey'){
            return blueGrey
        }
        if(props.buttonColor === 'brown'){
            return brown
        }
        if(props.buttonColor === 'common'){
            return common
        }
        if(props.buttonColor === 'cyan'){
            return cyan
        }
        if(props.buttonColor === 'deepOrange'){
            return deepOrange
        }
        if(props.buttonColor === 'deepPurple'){
            return deepPurple
        }
        if(props.buttonColor === 'grey'){
            return grey
        }
        if(props.buttonColor === 'indigo'){
            return indigo
        }
        if(props.buttonColor === 'lightBlue'){
            return lightBlue
        }
        if(props.buttonColor === 'lightGreen'){
            return lightGreen
        }
        if(props.buttonColor === 'lime'){
            return lime
        }
        if(props.buttonColor === 'orange'){
            return orange
        }
        if(props.buttonColor === 'pink'){
            return pink
        }
        if(props.buttonColor === 'teal'){
            return teal
        }
        if(props.buttonColor === 'yellow'){
            return yellow
        }
        else {
            return blue
        }
        
    }
    const ColorButton = withStyles((theme) => ({
        root: {
          color: theme.palette.getContrastText(getColor()[getTonalidade().contrast]),
          backgroundColor: getColor()[getTonalidade().color],
          '&:hover': {
            backgroundColor: getColor()[getTonalidade().hover],
          },
        },
      }))(Button);

      const ColorButtonOutilined = withStyles((theme) => ({
        root: {
          color: getColor()[getTonalidade().color],
          borderColor: getColor()[getTonalidade().color],
          '&:hover': {
            borderColor: getColor()[getTonalidade().hover],
          },
        },
      }))(Button);

      const ColorButtonNone = withStyles((theme) => ({
        root: {
          color: getColor()[getTonalidade().color],
          '&:hover': {
            // borderColor: getColor()[getTonalidade()+100],
          },
        },
      }))(Button);

    return(
    <>
    { props.variant === 'outlined' ?
    <ColorButtonOutilined className={props.className} style={props.style} variant="outlined" disableElevation disabled={props.disabled} size={props.size} onClick={props.onClick}>
        {props.children}
    </ColorButtonOutilined > : props.variant === 'none' ?
    <ColorButtonNone className={props.className} style={props.style} disableElevation disabled={props.disabled} size={props.size} onClick={props.onClick}>
        {props.children}
    </ColorButtonNone> : 
    <ColorButton className={props.className} style={props.style} disableElevation disabled={props.disabled} size={props.size} onClick={props.onClick}>
        {props.children}
    </ColorButton>
    }
    </>
    )
}
    