import React, { useState, useEffect } from 'react';
import {Modal, Spinner} from 'react-bootstrap'
import ButtonTemplate from '../Button/Button';
import "./ModalYN.css"


export function ModalYN(props){
    // var load = false
    const [load, setLoad] = useState(false)
  
    function yes(){
      setLoad(true)
      props.yes()
      hide()
    }

    function no(){
      props.no()
      hide()
    }
    function hide(){
      props.hide()
    }

    return(
        <>
         <Modal
        size="sm"
        show={props.show}
        centered
        onHide={hide}
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-sm">
            {props.title}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body style={{display: 'inline-flex', margin: 'auto'}}>
          <div  className="buttonYN">
              <ButtonTemplate size="large" buttonColor="red" onClick={e => no()} >
                Não
              </ButtonTemplate>
          </div>
          <div className="buttonYN">
              <ButtonTemplate size="large" buttonColor="teal" tonalidade="400" onClick={e=> yes()}>
                {
                  load === true ?
                 <Spinner variant="light" size="sm" animation="border" />
                 :   <div>Sim</div>
                }
                
              </ButtonTemplate>
          </div>
          </Modal.Body>
      </Modal>
        </>
    )
}

