import React from 'react';
import {Modal} from 'react-bootstrap'
import ButtonTemplate from '../Button/Button';
import "./ModalOk.css"


export function ModalOk(props){

  function ok(){
    props.ok()
    props.hide()
  }

    return(
        <>
         <Modal
        size="sm"
        show={props.show}
        centered
        onHide={props.hide}
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <Modal.Header closeButton>
          <Modal.Title id="example-modal-sizes-title-sm">
            {props.title}
          </Modal.Title>
        </Modal.Header>
        <Modal.Body style={{margin: 'auto'}}>
          <div className="buttonYN">
          <ButtonTemplate size="large" buttonColor="teal" tonalidade="400" onClick={e=> ok()}>
                Ok
              </ButtonTemplate>
          </div>
          </Modal.Body>
      </Modal>
        </>
    )
}

