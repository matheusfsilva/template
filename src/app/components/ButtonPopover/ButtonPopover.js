import React  from "react";
import Popover from 'react-bootstrap/Popover'
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'

export default function ButtonPopover(props) {

return(
<OverlayTrigger
            trigger="click"
            key={props.placement}
            placement={props.placement}
            overlay={
          <Popover id={`popover-positioned-${props.placement}`}>
                <Popover.Title as="h3">{props.title}
                </Popover.Title>
                    <Popover.Content>
                    {props.content}
                    </Popover.Content>
          </Popover>}>
        <div style={{maxWidth: 'max-content', display:'inline-flex'}}>
        {props.children}
        </div>
</OverlayTrigger>

)
}