import React, {useState, useEffect} from 'react';
import Tabs from '@material-ui/core/Tabs';
import Tab from '@material-ui/core/Tab';
import Paper from '@material-ui/core/Paper';


export default function TabsTemplate(props) {
const [value, setValue] = useState(0)

    useEffect(()=>{

    }, [])

    const changeMenu = (index) => {
        setValue(index)
    }

return(
    <Paper square>
              <Tabs 
                value={ value }
                indicatorColor="primary"
                className="tabs"
                onChange={changeMenu}
                variant="fullWidth"
                textColor="secondary"
                indicatorColor="secondary"
              >
            {
                props.options.map((el, i)=>(
                    <Tab disabled={el?.disabled} key={i} label={el.label} index={i} onClick={ () => changeMenu(i)}/>
                ))
            }
            </Tabs>
        </Paper>
)
}