import React, { useEffect, useState } from "react";
import { Grid } from "@material-ui/core";
import PaginationComponent from "../../components/Pagination/Pagination";
import ButtonTemplate from "../../components/Button/Button";
import AddIcon from '@material-ui/icons/Add';
import ItensPage from "../../components/ItensPage/ItensPage";
import TabsTemplate from "../../components/TabsTemplate/TabsTemplate";
import Select from 'react-select';
import ModalTemplate from "../../components/Modal/Modal";
import makeAnimated from 'react-select/animated';
import { ModalYN } from "../../components/ModalYN/ModalYN";
import { ModalOk } from "../../components/ModalOk/ModalOk"
import InputTemplate from '../../components/input/inputTemplate'
import SnackBar from '../../components/SnackBar/SnackBar'
import { EncryptStorage } from 'encrypt-storage';
import { Provider, atom, useAtom } from "jotai";

const animatedComponents = makeAnimated();

/*
Caminho para alterar header:
src/_metronic/layout/components/header/Topbar.js

caminho para menu lateral
src/_metronic/layout/components/aside/aside-menu/AsideMenuList.js
*/
export const encryptStorage = EncryptStorage('secret_key', {
    prefix: '@prefix'
  });



export default function ExComponentsPage(props) {
    const [selectOpt, setSelectOpt] = useState()
    const [options, setOptions] = useState([])
    const [showModal, setShowModal] = useState(false)
    const [showModalYN, setShowModalYN] = useState(false)
    const [showModalOk, setShowModalOk] = useState(false)
    const [openSnack, setOpenSnack] = useState(false)

    const TabelaTeste = [{
        nome: 'Matheus',
        cpf: '123',
        altura: '1,70cm',
        peso: 70
    }, {
        nome: 'Paulo',
        cpf: '234',
        altura: '1,80cm',
        peso: 80
    }, {
        nome: 'Ana',
        cpf: '541',
        altura: '1,60cm',
        peso: 64
    }, {
        nome: 'João',
        cpf: '222',
        altura: '1,70cm',
        peso: 72
    }]
    
    useEffect(()=>{
        options.push({label: 'Todos', value: -1})
        TabelaTeste.map((e,i)=>{options.push({label: e.nome, value: i, pessoa: e})})
        console.log(encryptStorage.getItem('user'));
        console.log(encryptStorage['user'])
        // const textLenAtom = atom((get) =>);
        // console.log(textLenAtom)
    })

    const changePage = (page) => {
        console.log(page)
      }
    
    const changeItens = (itens) => {
        console.log(itens)
    }

    const yes = () => {
        console.log('yes')
    }

    const no = () => {
        console.log('no')
    }

    const ok = () => {
        console.log('ok')
    }

return(
    <>
    <Grid container spacing={2} className="mt-6 mb-6 grid">
    <Grid item xs={12} sm={6}>
        <h2>Buttons</h2>
        <ButtonTemplate buttonColor="blue" tonalidade="500" size="large">
            <AddIcon/>Adicionar
        </ButtonTemplate>
        <ButtonTemplate buttonColor="green" variant="outlined" tonalidade="a100" size="small">
            Outilined
        </ButtonTemplate>
        <ButtonTemplate buttonColor="red" variant="none" tonalidade="500" size="medium">
            none
        </ButtonTemplate>
        <a target='_blank'  href="https://material-ui.com/customization/color/">
        <ButtonTemplate buttonColor="cyan" tonalidade="400" size="medium">
            cores
            {/* https://material-ui.com/customization/color/ */}
        </ButtonTemplate>
        </a>
    </Grid>
    <Grid item xs={12} sm={6}>
    <h2>Pagination</h2>
            <PaginationComponent itensPage={10} total={200} InicialPage={1} change={changePage}/>
    </Grid>
    <Grid item xs={12} sm={6}>
    <h2>Itens page</h2>
        <ItensPage change={changeItens}/>
    </Grid>
    <Grid item xs={12}>
    <h2>Tabs</h2>
        <TabsTemplate options={[{label: 'tab1'}, {label: 'tab2', disabled: true}, {label: 'tab3'}, {label: 'tab4'}]} /> 
        <h2>Tabs</h2>
        <div style={{maxWidth: '600px'}}><TabsTemplate options={[{label: 'tab1'}, {label: 'tab2', disabled: true},  {label: 'tab4'}]} /> </div>
    </Grid>
    <Grid item xs={12} sm={6}>
    <h2>Select 1</h2>
    <Select 
             components={animatedComponents}
             name="data"
             value={selectOpt}
             onChange={e => setSelectOpt(e)}
             placeholder={'Selecione'}
             options={options}
            />
    </Grid>
    <Grid item xs={12} sm={6}>
    <h2>Select multi</h2>
    <Select 
            isMulti
            components={animatedComponents}
             name="data"
             onChange={e => console.log(e)}
             placeholder={'sem competências'}
             options={options}
            />
    </Grid>
    <Grid item xs={12} sm={6}>
    <h2>Modal 1</h2>
    <ModalTemplate title="Modal 1" onHideAction={()=>console.log('ação hide')} hide={()=>setShowModal(false)} show={showModal}>
        <div>Conteudo do modal</div>
    </ModalTemplate>
    <ButtonTemplate buttonColor="grey" tonalidade="700" onClick={()=> setShowModal(true)}> Show Moldal</ButtonTemplate>

    <h2>Modal Y/N</h2>
    <ModalYN yes={yes} no={no} show={showModalYN} title="deseja continuar?" hide={()=>setShowModalYN(false)}></ModalYN>
    <ButtonTemplate buttonColor="grey" tonalidade="700" onClick={()=> setShowModalYN(true)}> Show Moldal Y/n</ButtonTemplate>

    <h2>Modal Y/N</h2>
    <ModalOk ok={ok} show={showModalOk}  hide={()=>setShowModalOk(false)} title="realizado com sucesso" ></ModalOk>
    <ButtonTemplate buttonColor="grey" tonalidade="700" onClick={()=> setShowModalOk(true)}> Show Moldal Y/n</ButtonTemplate>
    </Grid>

    <Grid item xs={12} sm={6}>
    <h2>Inputs </h2>
        <InputTemplate
          type="number"
        //   value={}
          onChange={e=> console.log(e.target.value)}
        />
         <InputTemplate
        //   value={}
          onChange={e=> console.log(e.target.value)}
        />
         <InputTemplate
          error={true}
        //   value={}
          onChange={e=> console.log(e.target.value)}
        />
         <InputTemplate
          error={true}
          errorMessenge="Campo requerido"
        //   value={}
          onChange={e=> console.log(e.target.value)}
        />
         <InputTemplate
         id="date"
         type="date"
        //   value={}
          onChange={e=> console.log(e.target.value)}
        />
    </Grid>
    <Grid item xs={12} sm={6}>
        <ButtonTemplate buttonColor="grey" tonalidade="700" onClick={()=> setOpenSnack(true)}> Show Snack</ButtonTemplate>
        <SnackBar open={openSnack}/>
    </Grid>
    </Grid>
    </>
)
}