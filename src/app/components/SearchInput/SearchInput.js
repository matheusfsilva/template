import React from 'react';
import "./SearchInput.css"
import SearchIcon from '@material-ui/icons/Search';
import OutlinedInput from '@material-ui/core/OutlinedInput';
import InputAdornment from '@material-ui/core/InputAdornment';

export function SearchInput(props){

return(
    <>
        <OutlinedInput
                color="secondary"
                variant="outlined"
                fullWidth
                placeholder="Pesquisar"
                placeholder={props.placeHolder}
                onChange={e => props.filter(e.target.value)}
                startAdornment={<InputAdornment position="start"><SearchIcon/></InputAdornment>}
              />
    </>
)
}