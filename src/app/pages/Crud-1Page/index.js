import React, {  useState, useEffect }  from "react";
import Grid from '@material-ui/core/Grid';
import InputTemplate from '../../components/input/inputTemplate'
import './crud-1.css'
import ButtonIconTemplate from "../../components/ButtonIcon/ButtonIcon";
import ButtonTemplate from "../../components/Button/Button"
import Loading from "../../components/Loading/Loading"
import {Paper, Table, TableBody, TableCell, TableRow, TableContainer, TableHead, Checkbox, LinearProgress} from '@material-ui/core';
import AddIcon from '@material-ui/icons/Add';
import DeleteIcon from '@material-ui/icons/Delete'
import ModalTemplate from "../../components/Modal/Modal";
import { AlertError } from "../../components/Alert/AlertError";
import { apiGETDependentes, apiPOSTPerson, apiPUTPerson, apiGETPerson } from "./actions.js"
import { NavLink } from "react-router-dom";
import { useHistory } from "react-router-dom";


export default function Crud1Page (props){
  let history = useHistory();

  const [showModal, setShowModal] = useState(false)
  const [selct, setSelect] = useState([])
  const [change, set] = useState(0)
  const [alertPresent, setAlertPresent] = useState(false)
  const [Person, setPerson] = useState({
    Nome: '',
    Sobrenome: '',
    Idade: 0,
    Data: '',
    Dependentes: []  
  })
  const [dependentesTable, setDependentesTable] = useState([])
  const [personcheck, setPersoncheck] = useState({
    Nome: false,
    Sobrenome: false,
    Idade: false,
    Data: false
  })
  const [load, setLoad] = useState(false)
  const [saveLoad, setSaveLoad] = useState(false)

  useEffect(()=>{
    async function get() {
      if(props.match.params.id!== 'add'){
        setLoad(true)
        let ps = await apiGETPerson(props.match.params.id)
        setPerson(ps)
        setLoad(false)
      }
    } get()
  }, [])

  async function showModalDependentes () {
    let dt = await apiGETDependentes()
    setSelect([])
    setDependentesTable(dt)
    setShowModal(true)
  }

  function updateList(index, element) {
      selct[index] = element.target.checked
  }

  function addDependentes() {
    let aux = []
    dependentesTable.forEach((e, i)=> {
      if(selct[i]){
        if(!Person.Dependentes.includes(e)){
          aux.push(e)
        }
      }
    })
    console.log(aux)
    setPerson((prevState) => (
      { ...prevState, Dependentes: Person.Dependentes.concat(aux) }
    ));
    setShowModal(false)
  }

  function deleteDependentes(index) {
    Person.Dependentes.splice(index, 1) 
    set(change+1)
   }

   function checagem() {
   let error = personcheck
     if((Person.Nome==='') || (Person.Nome===null)){
      setPersoncheck((prevState) => (
        { ...prevState, Nome: true }
      ));
      error.Nome = true
     } else {
      setPersoncheck((prevState) => (
        { ...prevState, Nome: false }
      ));
      error.Nome = false
     }
     if((Person.Sobrenome==='') || (Person.Sobrenome===null)){
      setPersoncheck((prevState) => (
        { ...prevState, Sobrenome: true }
      ));
      error.Sobrenome = true
     } else {
      setPersoncheck((prevState) => (
        { ...prevState, Sobrenome: false }
      ));
      error.Sobrenome = false
     }
     if((Person.Idade==='') || (Person.Idade===null)){
      setPersoncheck((prevState) => (
        { ...prevState, Idade: true }
      ));
      error.Idade = true
     } else {
      setPersoncheck((prevState) => (
        { ...prevState, Idade: false }
      ));
      error.Idade = false
     }
     if((Person.Data==='') || (Person.Data===null) || (new Date(Person.Data)===undefined)){
      setPersoncheck((prevState) => (
        { ...prevState, Data: true }
      ));
      error.Data = true
     } else {
      setPersoncheck((prevState) => (
        { ...prevState, Data: false }
      ));
      error.Data = false
     }
     if((error.Nome===false) || (error.Sobrenome===false) || (error.Idade===false) || (error.Data===false)){
       if(props.match.params.id==='add'){
        salvar()
       }else{
        salvarEdit()
       }
     }
   }

   async function salvarEdit() {
    setSaveLoad(true)
    let resp = await  apiPUTPerson(Person)
    if(resp === false){
      setAlertPresent(true)
      setTimeout(()=>{
      setAlertPresent(false)
      }, 8000)
      setSaveLoad(false)
    }
    if(resp === true){
       setSaveLoad(false)
       history.push('/Lista-1')
    }
   }

   async function salvar() {
    setSaveLoad(true)
    let resp = await  apiPOSTPerson(Person)
    if(resp === false){
      setAlertPresent(true)
      setTimeout(()=>{
        setAlertPresent(false)
      }, 8000)
      setSaveLoad(false)
    }
    if(resp === true){
      setSaveLoad(false)
      history.push('/Lista-1')
    }
   }

return(
    <>
    <h1> Editar colaborador </h1>
    <div style={{position: 'relative'}}>
    {
      load ? <Loading/>:null
    }
    <Grid container spacing={2} className="mt-6 mb-6 grid">
    <Grid item xs={12} sm={4}>
        <h6>Nome</h6>
        <InputTemplate
          error={personcheck.Nome}
          // errorMessenge="Campo requerido"
          placeholder="Nome"
          value={Person?.Nome}
          onChange={(e)=> setPerson((prevState) => (
            { ...prevState, Nome: e.target.value }
          ))}
        />
    </Grid>

    <Grid item xs={12} sm={4}>
        <h6>Sobrenome</h6>
        <InputTemplate
          error={personcheck.Sobrenome}
          // errorMessenge="Campo requerido"
          placeholder="Sobrenome"
           value={Person?.Sobrenome}
          onChange={(e)=> setPerson((prevState) => (
            { ...prevState, Sobrenome: e.target.value }
          ))}
        />
    </Grid>

    <Grid item xs={12} sm={4}>
        <h6>idade</h6>
        <InputTemplate
          error={personcheck.Idade}
          // errorMessenge="Campo requerido"
          type="number"
          value={Person?.Idade}
          onChange={(e)=> setPerson((prevState) => (
            { ...prevState, Idade: e.target.value }
          ))}
        />
    </Grid>


    <Grid item xs={12} sm={8}>
    <h6>Dependentes</h6>
    <Paper elevation={0}>
    <TableContainer>
    <Table>
        <TableHead>
          <TableRow>
            <TableCell>Nome</TableCell>
            <TableCell>Cpf</TableCell>
            <TableCell></TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
         {( Person.Dependentes ).map((object, i)=>(
            <TableRow key={i}>
            <TableCell>{object.Nome}</TableCell>
            <TableCell>{object.Cpf}</TableCell>
            <TableCell>
              <ButtonIconTemplate buttonColor="red" tonalidade="500" icon={<DeleteIcon fontSize="large"/>} onClick={()=>deleteDependentes(i)}/>
            </TableCell>
          </TableRow>
          ))}
          </TableBody>
          </Table>
    </TableContainer>
    </Paper>
    <ButtonIconTemplate className="btAdd" buttonColor="blue" variant="contained" tonalidade="500" icon={<AddIcon/>} onClick={()=>showModalDependentes() }/>
    </Grid>

    <Grid item xs={12} sm={4}>
        <h6>Data de nascimento</h6>
        <InputTemplate
          error={personcheck.Data}
          id="date"
          type="date"
          variant="outlined"
          value={Person.Data !== '' ? new Date(Person.Data).toISOString().slice(0,10): Person.Data}

          onChange={(e)=> setPerson((prevState) => (
            { ...prevState, Data: e.target.value }
          ))}
        />
    </Grid>
    
    <div className="bts">
      { saveLoad ?
        <LinearProgress color="secondary" />:null
      }
    <NavLink to={'/Lista-1'}>
          <ButtonTemplate style={{marginRight: '5px'}} buttonColor="red" size="large" tonalidade="A700">Cancelar</ButtonTemplate>
    </NavLink>
          <ButtonTemplate buttonColor="teal" tonalidade="A700" size="large" onClick={()=> checagem()}>Salvar</ButtonTemplate>
    </div>
    </Grid>
    </div>

    {
      alertPresent ? 
      <AlertError menssagem="Erro ao salvar"></AlertError> : null
    }

    <ModalTemplate hide={()=>setShowModal(false)} show={showModal} title="Adicionar dependente" size="xl">
    <Paper elevation={0}>
    <TableContainer>
    <Table>
        <TableHead>
          <TableRow>
            <TableCell></TableCell>
            <TableCell>Nome</TableCell>
            <TableCell>Cpf</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
         {( dependentesTable ).map((object, i)=>(
            <TableRow key={i}>
            <TableCell><Checkbox value={selct[i]} onChange={(e)=> updateList(i,e)}/></TableCell>
            <TableCell>{object.Nome}</TableCell>
            <TableCell>{object.Cpf}</TableCell>
          </TableRow>
          ))}
          </TableBody>
          </Table>
    </TableContainer>
    <div style={{width: '100%'}}>
      <div className="btAddonTable">
      <ButtonTemplate  buttonColor="blue" onClick={()=>addDependentes()}>
        <AddIcon/> Adicionar
      </ButtonTemplate>
      </div>
    </div>
    </Paper>
    </ModalTemplate>
    </>
);
}
//dependentesTable