import React from "react";
import OutlinedInput from '@material-ui/core/OutlinedInput';
import { TextField } from "@material-ui/core";
import Paper from '@material-ui/core/Paper';
import { makeStyles  } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    size: {
      height: ' 3.5876em  !important',
    },
    backDisabled: {
        backgroundColor: '#e4e6ef'
    },
    error: {
        color: '#f018a6'
    }
}));

export default function InputTemplate (props) {
    const classes = useStyles()
    
return(
    <>
    <Paper elevation={0} className={props.disabled ? classes.backDisabled: null}>
        <OutlinedInput
        className={classes.size}
        color="secondary"
        error={props.error}
        // variant="outlined"
        fullWidth
        type={props.type}
        disabled={props.disabled}
        value={props.value}
        placeholder={props.placeholder}
        onChange={props.onChange}
        />
    </Paper>
    { props.error ? 
        <div className={classes.error}>{props.errorMessenge}</div>:null
    } 
    </>
)
}