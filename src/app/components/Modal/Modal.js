import React from 'react';
import {Modal} from 'react-bootstrap'
import CloseIcon from '@material-ui/icons/Close';
import IconButton from '@material-ui/core/IconButton';

export default function ModalTemplate(props) {

function hide(){
  if(props.onHideAction){
    props.onHideAction()
  }
  props.hide()
}

return(
    <Modal
   
         dialogClassName="modal-90w"
        size={props.size}
        show={props.show}
        centered
        onHide={hide}
        aria-labelledby="example-modal-sizes-title-sm"
      >
        <Modal.Header>
          <Modal.Title id="example-modal-sizes-title-sm">
            {props.title} 
          </Modal.Title>
          <IconButton aria-label="delete" style={{marginLeft: 'auto'}} onClick={(e)=>hide()}>
                                <CloseIcon />
                            </IconButton>
        </Modal.Header>
        <Modal.Body>
            {props.children}
        </Modal.Body>
    </Modal>
)
}