import React, {  useState }  from "react";
import {  Card, DropdownButton, Dropdown} from 'react-bootstrap'
import "./ItensPage.css"

export default function ItensPage (props) {
const [itensPag, setItensPag] = useState(10)


function changeItensPage(itens) {
  setItensPag(itens)
  props.change(itens)
}

return(
    <>
    <div className="flex1">
    <Card className="itemsPpage">
      <DropdownButton  variant="Secondary" id="dropdown-basic-button" title={itensPag}>
        <Dropdown.Item  onClick={(e) => changeItensPage(5)} href="#/action-1"><div >5</div></Dropdown.Item>
        <Dropdown.Item  onClick={(e) => changeItensPage(10)} href="#/action-2"><div >10</div></Dropdown.Item>
        <Dropdown.Item  onClick={(e) => changeItensPage(15)} href="#/action-3"><div >15</div></Dropdown.Item>
        <Dropdown.Item  onClick={(e) => changeItensPage(20)} href="#/action-3"><div >20</div></Dropdown.Item>
        
      </DropdownButton>
      <div className="labelip">itens por pagina </div>
      </Card>
      </div>
    </>
    )
}


{/* <Card className="itemsPpage">
      <DropdownButton  variant="Secondary" id="dropdown-basic-button" title={itensPag}>
        <Dropdown.Item  onClick={(e) => changeItensPage(5)} href="#/action-1"><div >5</div></Dropdown.Item>
        <Dropdown.Item  onClick={(e) => changeItensPage(10)} href="#/action-2"><div >10</div></Dropdown.Item>
        <Dropdown.Item  onClick={(e) => changeItensPage(15)} href="#/action-3"><div >15</div></Dropdown.Item>
        <Dropdown.Item  onClick={(e) => changeItensPage(20)} href="#/action-3"><div >20</div></Dropdown.Item>
        
      </DropdownButton>
      <div className="labelip">itens por pagina </div>
      </Card> */}