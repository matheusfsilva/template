import React, { useState, useEffect } from 'react';
import Snackbar from '@material-ui/core/Snackbar';
import MuiAlert from '@material-ui/lab/Alert';
import "./Alert.css"

function AlertMu(props) {
    return <MuiAlert elevation={10} variant="filled" {...props} />;
  }

export function Alert(props){
    const [open, setOpen] = useState(false);

    useEffect(function(){
        setOpen(true)   
    })


    return(
        <>
            <Snackbar open={open}>
              <AlertMu severity="success">
                {props.menssagem}
              </AlertMu>
            </Snackbar>
        </>
    )
}

